package main

import "fmt"

func main(){
	var x string
	x = "first"
	fmt.Println(x)
	x = x + " second"
	fmt.Println(x)

	var v string = "Hi"
	var y string = " there"
	fmt.Println(v == y)
}