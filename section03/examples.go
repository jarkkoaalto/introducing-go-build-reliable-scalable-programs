package main
import "fmt"
var x string = "Hello, world"

func main(){
	// What is to ways to create variable?
	//write variable outside func or 2 inside
	var y string
	y = "Hello world"
	fmt.Println(x + " " + y)

	//  What is the value of x after running := 5, x += 1?
	var u int = 0
	u = 5
	fmt.Println(u)
	u += 1
	fmt.Println(u)

	// Using the example program as a starting point, write the coverts from
	// Fahrenheit into Celsius
	fmt.Println("This section transfer F to C degrees")
	fmt.Println("Enter a Fahrenheit: ")
	var input float64
	fmt.Scanf("%f", &input)

	output := ((input-32)*5/9)
	fmt.Println("F to C = ", output)

	
	

}