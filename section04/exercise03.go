package main
import "fmt"

// Write a program that prints the number of 1 to 100 but for multibles
// of threee "Fizz" instead of the number, and for the multibles of 
// five

func main(){
	for i := 0; i<=100; i=i+5{
		fmt.Println("Fizz")
	}
}

