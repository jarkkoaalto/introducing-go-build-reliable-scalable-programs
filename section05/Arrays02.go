package main
import "fmt"

func main(){
	var x [5] float64
	x[0] = 98
	x[1] = 93
	x[2] = 77
	x[3] = 29
	x[4] = 12

	var total float64 = 0
	for i := 0; i<5; i++ {
		total += x[i]
	}
	// print average of a series of test score
	fmt.Println(total / 5)

	// In this loop i represent the current position in the array and value is the same as x[i]
	// for i, value := range x{
	// 	total += value
	//}
	// fmt.Println(total / float64(len(x)))

	// The go compiler won't allow you to create variable that you never use. 
	// Because we don't use i inside of our loop, we need to change it to this.
	// single underscore _ is used to tell the compiler that we don't need this.
	var total1 float64 = 0
	for _, value := range x{
		total1 +=value
	}
	fmt.Println(total1/float64(len(x)))
}