package main
import "fmt"

func main(){
	elements := make(map[string] string)
	elements["H"] = "Hydrogen"
	elements["He"] = "Helium"
	elements["Li"] = "Lithium"
	elements["Be"] = "Beryllium"
	elements["B"] = "Boron"
	elements["C"] = "Carbon"
	elements["O"] = "Oxygen"
	elements["Ne"] = "Neon"

	fmt.Println(elements["Li"])
}