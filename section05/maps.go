package main
import "fmt"

func main() {

	// String
	x := make(map[string]int)
	x["key"] = 10
	fmt.Println(x["key"])

	//int
	y := make(map[int]int)
	y[1] = 10
	fmt.Println(y[1])
}