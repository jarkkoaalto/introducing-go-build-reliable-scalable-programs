# Introducing-Go-Build-Reliable-Scalable-Programs

Introducing Go Build Reliable, Scalable Programs - book

Book content

###### Chapter 1. Getting Started
###### Chapter 2. Types
###### Chapter 3. Variables
###### Chapter 4. Control Structures
###### Chapter 5. Arrays, Slices, and Maps
###### Chapter 6. Functions
###### Chapter 7. Structs and Interfaces
###### Chapter 8. Packages
###### Chapter 9. Testing
###### Chapter 10. Concurrency
###### Chapter 11. Next Steps