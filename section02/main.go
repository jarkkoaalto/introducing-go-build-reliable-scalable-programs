package main

import "fmt"

func main(){
	fmt.Println("2 + 4 = ", 2+4)
	fmt.Println("2 - 4 = ", 2-4)
	fmt.Println("2 / 4 = ", 2.0/4.0)
	fmt.Println("2 * 4 = ", 2*4)
	fmt.Println("2 % 4 = ", 2%4)
}