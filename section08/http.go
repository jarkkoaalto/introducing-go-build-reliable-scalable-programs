package main

import (
	"io"
	"net/http"
)

// VALIDATION CHECK USE http://localhost:9000/hello

func hello(res http.ResponseWriter, req *http.Request) {
	res.Header().Set(
		"Content-Type",
		"text/html",
	)
	io.WriteString(
		res,
		`<DOCTYPE html>
<html>
<head>
<title>Hello, World</title>
</head>
<body>
Hello, World!
</body>
</html>`,
	)
	http.Handle(
		"/assets/",
		http.StripPrefix(
			"/assets/",
			http.FileServer(http.Dir("assets")),
		),
	)
}
func main() {
	http.HandleFunc("/hello", hello)
	http.ListenAndServe(":9000", nil)

}
