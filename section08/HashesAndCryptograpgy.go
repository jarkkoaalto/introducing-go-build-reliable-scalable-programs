package main

import "fmt"
import "hash/crc32"

func main() {
	// Create a hash
	hash := crc32.NewIEEE()
	// Write data to it
	hash.Write([]byte("This is secred test"))

	// calculate the crc32 checksum
	chs := hash.Sum32()
	fmt.Println(chs)
}
