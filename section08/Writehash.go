package main

import (
	"fmt"
	"hash/crc32"
	"io"
	"os"
)

func getHash(filename string) (uint32, error) {
	// Open the file
	f, err := os.Open(filename)
	if err != nil {
		return 0, err
	}

	// Close always open files
	defer f.Close()

	// create hash
	h := crc32.NewIEEE()

	// Copy to the file into the hasher- copy takes (dst, src) and returns (byteWritten, error)
	_, err := io.Copy(h, f)

	// We don't care about how many bytes were written, but we do want to handle the error
	if err != nil {
		return 0, err
	}
	return h.Sum32(), nil
}

func main() {
	h1, err := getHash("test1.txt")
	if err != nil {
		return
	}
	h2, err := getHash("test2.txt")
	if err != nil {
		return
	}
	fmt.Println(h1, h2, h1 == h2)
}
