package main

import (
	"fmt"
	"strings"
)

func main() {
	// func toUpper(s string) string
	fmt.Println(strings.ToUpper("test"))
	// => "TEST"
}
