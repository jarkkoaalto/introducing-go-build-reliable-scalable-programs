package main

import (
	"fmt"
	"strings"
)

// To find position of a small string in bigger string,
// use the Index function. Function retturns -1 if not found

func main() {
	// func index(s, sep string) int
	fmt.Println(strings.Index("test", "e"))
}
