package main

import (
	"fmt"
	"strings"
)

// To replace a smaller string in a bigger string with some other string,
// use the Replace function. In go, replace also takes a number indicating
// how many times to do the replacement (pass -1 to do it as many times as poissible)
func main() {
	// Func Report(s string, count int) string
	fmt.Println(strings.Repeat("a", 5))
}
