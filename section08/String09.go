package main

import (
	"fmt"
	"strings"
)

// To covert a string to all lowercase letters, use the ToLower function:
func main() {
	// func split(s, sep string) [] string
	fmt.Println(strings.Split("a-b-c-d-e", "-"))
	// -> [] string{"a","b","c","d","e"}
}
