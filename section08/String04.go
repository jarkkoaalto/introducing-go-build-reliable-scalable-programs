package main

import (
	"fmt"
	"strings"
)

// Determine if a bigger string ends with a smaller string, use the HasSuffix function:

func main() {
	// func HasSuffix(s, suffix string) bool
	fmt.Println(strings.HasSuffix("test", "st"))
}
