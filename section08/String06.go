package main

import (
	"fmt"
	"strings"
)

// To take a list of strings and join them together in a single string
// separated by another string (e.g. a comma), use the join function.
func main() {
	// func Join(a [] string, sep string) string
	fmt.Println(strings.Join([]string{"a", "b"}, "-"))
}
