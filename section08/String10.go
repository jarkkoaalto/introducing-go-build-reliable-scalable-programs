package main

import (
	"fmt"
	"strings"
)

// to convert a string to all uppercase letters, use the ToUpper function:
func main() {
	//func ToLower(s string) string
	fmt.Println(strings.ToLower("TEST"))
	// => "test"
}
